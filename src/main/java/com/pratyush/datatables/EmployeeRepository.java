package com.pratyush.datatables;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends DataTablesRepository<Employee, Integer>{
	
}