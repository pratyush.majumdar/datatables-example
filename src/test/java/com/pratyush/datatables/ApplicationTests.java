package com.pratyush.datatables;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ApplicationTests {
	
	@Autowired
	private EmployeeController employeeController;
	
	@Test
	void testControllers() throws Exception {
		assertThat(employeeController).isNotNull();
	}
}
