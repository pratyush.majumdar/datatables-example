# datatables-example
DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, built upon the foundations of progressive enhancement, that adds all of these advanced features to any HTML table. Spring Data JPA Datatables extension is used to work with the jQuery plug-in DataTables.

### Technologies Used
- Maven
- SpringBoot
- Spring Data JPA
- Spring Data JPA Datatables
- MySQL Database
- Thymeleaf


### MySQL table Structure
```sql
CREATE TABLE `employee` (
  	`id` bigint(20) NOT NULL AUTO_INCREMENT,
  	`email` varchar(255) DEFAULT NULL,
  	`first_name` varchar(255) DEFAULT NULL,
  	`last_name` varchar(255) DEFAULT NULL,
  	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

### Run Command
```shell
mvn spring-boot:run
```

### Alternative set of Commands
```shell
mvn clean package spring-boot:repackage
java -jar target/datatables-example-0.0.1-SNAPSHOT.jar
```

### Reference
https://github.com/darrachequesne/spring-data-jpa-datatables